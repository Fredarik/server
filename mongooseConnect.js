const mongoose = require('mongoose');
const { MDURL } = require('./constants');

module.exports = () => {
   return mongoose.connect(MDURL, {
      useUnifiedTopology: true,
      useNewUrlParser: true
   }).then((res) => {
      console.log('mongoDB connected');
      return res;
   }).catch((err) => {
      console.error('mongoDB does noe connect', err);
   });
}