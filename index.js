const express = require('express');
const constants = require('./constants');
const cors = require('cors');
const { termsRouter } = require('./routers/terms');
const mongooseConnect = require('./mongooseConnect');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));
app.use(cors());

mongooseConnect();

app.use('/terms', termsRouter);

app.listen(constants.PORT, () => {
   console.log('server is starting')
})