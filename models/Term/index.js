const mongoose = require('mongoose');

const termSchema = new mongoose.Schema({
   name: String,
   description: String
});

const TermModel = mongoose.model('Term', termSchema);

module.exports = TermModel;