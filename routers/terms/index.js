const express = require('express');
const TermModel = require('../../models/Term');

const termsRouter = new express.Router();

termsRouter.get('/', (req, res) => {
   console.log('init terms get');
   TermModel.find().then((_terms) => {
      res.send(_terms);
   })
})

termsRouter.post('/', (req, res) => {
   console.log('init terms post with body - ', req.body);
   const term = new TermModel(req.body);
   term.save().then(() => {
      console.log('term is saved');
      res.send(req.body);
   }).catch((err) => {
      console.log(err);
      throw new Error();
   })
   
})

termsRouter.delete('/:id', (req, res) => {
   TermModel.deleteOne({_id: req.params.id}).then(() => {
      return TermModel.find().then((_terms) => {
         console.log('term is deleted');
         res.send(_terms);
      })
   })
})
termsRouter.patch('/:id', (req, res) => {
   console.log('patch', req.params.id, req.body)
   TermModel.updateOne({_id: req.params.id}, req.body ).then(() => {
      console.log('term is changed');
      return TermModel.find().then((_terms) => {
         res.send(_terms);
      })
   })
})

module.exports = {
   termsRouter
};